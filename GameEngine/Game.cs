﻿using Enumerations;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GameEngine
{
    class Game
    {
        private Dictionary<Color, Agent> _agents;
        private Color _current_turn;
        private Board _board;

        public Game(Agent whiteAgent, Agent blackAgent)
        {
            _agents = new Dictionary<Color, Agent>();
            _agents.Add(Color.WHITE, whiteAgent);
            _agents.Add(Color.BLACK, blackAgent);
            _current_turn = Color.BLACK;
            _board = new Board();
        }

        public void Play()
        {

            while (_board.HasValidMoves)
            {
                DisplayCurrentState();
                Move m = _agents[_current_turn].GetNextMove(_board);
                bool validMove = false;
                if (m != null)
                {
                    if (_board.PlayMove(m))
                    {
                        validMove = true;
                    }
                }
                if (validMove)
                {
                    ChangeTurn();
                }
                else
                {
                    Console.WriteLine(String.Format("The {0} agent produced an invalid move or no move at all! Aborting.", _current_turn));
                    return;
                }
            }

        }

        private void ChangeTurn()
        {
            if (_current_turn == Color.WHITE)
            {
                _current_turn = Color.BLACK;
            }
            else
            {
                _current_turn = Color.WHITE;
            }
        }

        private void DisplayCurrentState()
        {
            Console.WriteLine("------------------------------------------");
            Console.WriteLine(String.Format("{0} player's turn", _current_turn));
            Console.WriteLine("------------------------------------------");
            Console.WriteLine(_board);
            _board.DumpValidMoves(_current_turn);
            Console.WriteLine("------------------------------------------");
        }

    }
}
