﻿using Enumerations;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GameEngine
{
    class Program
    {
        static void Main(string[] args)
        {
            if (args.Length != 3)
            {
                Console.WriteLine("Parameters : 1 - White agent path, 2 - Black agent path, 3 - output file path");
            }
            else
            {
                Agent.OUTPUT_FILENAME = args[2];
                Agent b = new Agent(args[1], Color.BLACK);
                Agent w = new Agent(args[0], Color.WHITE);
                if (b.CanAccessExe && w.CanAccessExe)
                {
                    Game g = new Game(w, b);
                    g.Play();
                }
                else
                {
                    if (!b.CanAccessExe)
                    {
                        Console.WriteLine("Black agent's path is invalid");
                    }
                    if (!w.CanAccessExe)
                    {
                        Console.WriteLine("White agent's path is invalid");
                    }
                }
            }

            


            Console.ReadKey();
        }
    }
}
