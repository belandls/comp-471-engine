﻿using Enumerations;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GameEngine
{
    class Agent
    {
        public static string OUTPUT_FILENAME;

        private ProcessStartInfo _agent_exe_info;
        private Color _color;
        private bool _can_access_exe;

        public bool CanAccessExe
        {
            get
            {
                return _can_access_exe;
            }
        }

        public Agent(string exePath, Color color)
        {
            _can_access_exe = false;
            if (File.Exists(exePath))
            {
                _agent_exe_info = new ProcessStartInfo(exePath);
                _agent_exe_info.CreateNoWindow = true;
                _agent_exe_info.UseShellExecute = true;
                _color = color;
                _can_access_exe = true;
            }
        }

        public Move GetNextMove(Board currentBoard)
        {
            if (_can_access_exe)
            {
                try
                {
                    _agent_exe_info.Arguments = (_color == Color.WHITE ? "W " : "B ") + currentBoard.GetParameterString() + " " + OUTPUT_FILENAME;
                    var p = Process.Start(_agent_exe_info);
                    p.WaitForExit();
                    if (File.Exists(OUTPUT_FILENAME))
                    {
                        string result = File.ReadLines(OUTPUT_FILENAME).First();
                        string[] stripped = result.Split(new char[] { ',' });
                        int rowTemp = int.Parse(stripped[0]);
                        int colTemp = int.Parse(stripped[1]);
                        File.Delete(OUTPUT_FILENAME);
                        return new GameEngine.Move(_color, rowTemp, colTemp);
                    }
                    else
                    {
                        return null;
                    }
                }
                catch (Exception ex)
                {
                    return null;
                }
            }
            return null;
            
        }
    }
}
