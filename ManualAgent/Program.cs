﻿using Enumerations;
using GameEngine;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ManualAgent
{
    class Program
    {
        static void Main(string[] args)
        {
            Color currentColor = (args[0] == "B" ? Color.BLACK : Color.WHITE);
            Board b = new Board(args[1]);

            Console.WriteLine(String.Format("Manual Agent for : {0}", currentColor));
            bool validMove = false;
            Move resultMove = null;
            int x,y;
            while (!validMove)
            {
                string temp;
                Console.WriteLine("Position X :");
                temp = Console.ReadLine();
                if (int.TryParse(temp, out x))
                {
                    Console.WriteLine("Position Y :");
                    temp = Console.ReadLine();
                    if (int.TryParse(temp, out y))
                    {
                        resultMove = new Move(currentColor, x, y);
                        validMove = b.ValidateMove(resultMove);
                    }
                }
            }

            File.WriteAllLines(args[2], new String[] { String.Format("{0},{1}", resultMove.Col, resultMove.Row) });
        }
    }
}
