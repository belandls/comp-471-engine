﻿using Enumerations;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GameEngine
{
    class Move
    {

        private Color _color;
        private int _row;
        private int _col;

        public Color MoveColor 
        {
            get
            {
                return _color;
            }
        }

        public int Row
        {
            get
            {
                return _row;
            }
        }

        public int Col
        {
            get
            {
                return _col;
            }
        }

        public Move(Color color, int col, int row)
        {
            _color = color;
            _row = row;
            _col = col;
        }

        public override bool Equals(object obj)
        {
            if (obj == null)
            {
                return false;
            }else if(obj.GetType() != typeof(Move))
            {
                return false;
            }
            else
            {
                Move temp = (Move)obj;
                return temp._color == _color && temp._col == _col && temp._row == _row;
            }
        }

        public override int GetHashCode()
        {
            return _color.GetHashCode() + _col.GetHashCode() + _row.GetHashCode();
        }

        public override string ToString()
        {
            return String.Format("{0} - [{1},{2}]", _color, _col, _row);
        }
    }
}
