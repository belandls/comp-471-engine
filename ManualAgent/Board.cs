﻿using Enumerations;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GameEngine
{
    class Board
    {
        private enum Direction
        {
            RIGHT,
            LEFT,
            UP,
            DOWN,
            DIAG_LU,
            DIAG_RU,
            DIAG_LD,
            DIAG_RD
        }

        private class Position
        {
            private const int BOARD_HEIGHT = 8;
            private const int BOARD_WIDTH = 8;
            private int _x;
            private int _y;

            public int X
            {
                get
                {
                    return _x;
                }
            }

            public int Y
            {
                get
                {
                    return _y;
                }
            }

            public Position(int x, int y)
            {
                _x = x;
                _y = y;
            }

            public Position Move(Direction d)
            {
                switch (d)
                {
                    case Direction.UP:
                        if (_y - 1 > 0)
                        {
                            return new Position(_x, _y - 1);
                        }
                        break;
                    case Direction.DOWN:
                        if (_y + 1 < BOARD_HEIGHT)
                        {
                            return new Position(_x, _y + 1);
                        }
                        break;
                    case Direction.LEFT:
                        if (_x - 1 > 0)
                        {
                            return new Position(_x - 1, _y);
                        }
                        break;
                    case Direction.RIGHT:
                        if (_x + 1 < BOARD_WIDTH)
                        {
                            return new Position(_x + 1, _y);
                        }
                        break;
                    case Direction.DIAG_LU:
                        if (_y - 1 > 0 && _x - 1 > 0)
                        {
                            return new Position(_x - 1, _y - 1);
                        }
                        break;
                    case Direction.DIAG_RU:
                        if (_y - 1 > 0 && _x + 1 < BOARD_WIDTH)
                        {
                            return new Position(_x + 1, _y - 1);
                        }
                        break;
                    case Direction.DIAG_LD:
                        if (_y + 1 < BOARD_WIDTH && _x - 1 > 0)
                        {
                            return new Position(_x - 1, _y + 1);
                        }
                        break;
                    case Direction.DIAG_RD:
                        if (_y + 1 < BOARD_WIDTH && _x + 1 < BOARD_HEIGHT)
                        {
                            return new Position(_x + 1, _y + 1);
                        }
                        break;
                }
                return null;
            }

        }

        private const int BOARD_HEIGHT = 8;
        private const int BOARD_WIDTH = 8;
        private Piece[,] _board;
        private Dictionary<Color, HashSet<Move>> _valid_moves;

        public bool HasValidMoves
        {
            get
            {
                return _valid_moves[Color.WHITE].Count > 0 && _valid_moves[Color.BLACK].Count > 0;
            }
        }

        public Board()
        {
            _board = new Piece[BOARD_WIDTH, BOARD_HEIGHT];
            _board[3, 3] = new Piece(Color.WHITE);
            _board[4, 4] = new Piece(Color.WHITE);
            _board[4, 3] = new Piece(Color.BLACK);
            _board[3, 4] = new Piece(Color.BLACK);
            _valid_moves = new Dictionary<Color, HashSet<Move>>();
            _valid_moves.Add(Color.BLACK, new HashSet<Move>());
            _valid_moves.Add(Color.WHITE, new HashSet<Move>());
            UpdateValidMoves();
        }

        public Board(string initialBoard)
        {
            _board = new Piece[BOARD_WIDTH, BOARD_HEIGHT];
            _valid_moves = new Dictionary<Color, HashSet<Move>>();
            _valid_moves.Add(Color.BLACK, new HashSet<Move>());
            _valid_moves.Add(Color.WHITE, new HashSet<Move>());
            for (int y = 0; y < BOARD_HEIGHT; ++y)
            {
                for (int x = 0; x < BOARD_WIDTH; ++x)
                {
                    if (initialBoard[y * BOARD_WIDTH + x] != '-')
                    {
                        if (initialBoard[y * BOARD_WIDTH + x] == 'B')
                        {
                            _board[x, y] = new Piece(Color.BLACK);
                        }
                        else
                        {
                            _board[x, y] = new Piece(Color.WHITE);
                        }
                    }
                }
            }
            UpdateValidMoves();
        }

        public bool ValidateMove(Move m)
        {
            return _valid_moves[m.MoveColor].Contains(m);
        }

        private void UpdateValidMoves()
        {
            _valid_moves[Color.BLACK].Clear();
            _valid_moves[Color.WHITE].Clear();
            for (int y = 0; y < BOARD_HEIGHT; ++y)
            {
                for (int x = 0; x < BOARD_WIDTH; ++x)
                {
                    Position p = new Position(x, y);
                    AddAnyValidMove(Color.BLACK, p, Direction.UP);
                    AddAnyValidMove(Color.BLACK, p, Direction.DOWN);
                    AddAnyValidMove(Color.BLACK, p, Direction.RIGHT);
                    AddAnyValidMove(Color.BLACK, p, Direction.LEFT);
                    AddAnyValidMove(Color.BLACK, p, Direction.DIAG_LU);
                    AddAnyValidMove(Color.BLACK, p, Direction.DIAG_RU);
                    AddAnyValidMove(Color.BLACK, p, Direction.DIAG_LD);
                    AddAnyValidMove(Color.BLACK, p, Direction.DIAG_RD);
                    AddAnyValidMove(Color.WHITE, p, Direction.UP);
                    AddAnyValidMove(Color.WHITE, p, Direction.DOWN);
                    AddAnyValidMove(Color.WHITE, p, Direction.RIGHT);
                    AddAnyValidMove(Color.WHITE, p, Direction.LEFT);
                    AddAnyValidMove(Color.WHITE, p, Direction.DIAG_LU);
                    AddAnyValidMove(Color.WHITE, p, Direction.DIAG_RU);
                    AddAnyValidMove(Color.WHITE, p, Direction.DIAG_LD);
                    AddAnyValidMove(Color.WHITE, p, Direction.DIAG_RD);

                }
            }
        }

        private void AddAnyValidMove(Color c, Position sourcePosition, Direction d)
        {
            Position nextPos = sourcePosition.Move(d);
            bool hasVisitedOnePiece = false;
            while (nextPos != null && _board[nextPos.X, nextPos.Y] != null && _board[nextPos.X, nextPos.Y].CurrentColor != c)
            {
                hasVisitedOnePiece = true;
                nextPos = nextPos.Move(d);
            }
            if (nextPos != null && _board[nextPos.X, nextPos.Y] != null && _board[nextPos.X, nextPos.Y].CurrentColor == c)
            {
                if (hasVisitedOnePiece)
                {
                    Move m = new Move(c, sourcePosition.X, sourcePosition.Y);
                    if (!_valid_moves[c].Contains(m))
                    {
                        _valid_moves[c].Add(m);
                    }
                }
            }
        }

        public void DumpValidMoves()
        {
            Console.WriteLine("Valid moves for WHITE");
            foreach (var m in _valid_moves[Color.WHITE])
            {
                Console.WriteLine(m);
            }
            Console.WriteLine("\nValid moves for BLACK");
            foreach (var m in _valid_moves[Color.BLACK])
            {
                Console.WriteLine(m);
            }
        }

        public bool PlayMove(Move m)
        {
            if (_valid_moves[m.MoveColor].Contains(m))
            {
                Piece p = new Piece(m.MoveColor);
                _board[m.Col, m.Row] = p;
                Position sp = new Position(m.Col, m.Row);
                FlipPieces(Direction.DIAG_LD, p, sp);
                FlipPieces(Direction.DIAG_RD, p, sp);
                FlipPieces(Direction.DIAG_LU, p, sp);
                FlipPieces(Direction.DIAG_RU, p, sp);
                FlipPieces(Direction.UP, p, sp);
                FlipPieces(Direction.DOWN, p, sp);
                FlipPieces(Direction.RIGHT, p, sp);
                FlipPieces(Direction.LEFT, p, sp);
                UpdateValidMoves();
                return true;
            }
            return false;
        }

        private void FlipPieces(Direction d, Piece sourcePiece, Position sourcePos)
        {
            Position nextPos = sourcePos.Move(d);
            List<Piece> piecesToFlip = new List<Piece>();
            while (nextPos != null && _board[nextPos.X, nextPos.Y] != null && _board[nextPos.X, nextPos.Y].CurrentColor != sourcePiece.CurrentColor)
            {
                //_board[nextPos.X, nextPos.Y].Flip();
                piecesToFlip.Add(_board[nextPos.X, nextPos.Y]);
                nextPos = nextPos.Move(d);
            }
            if (nextPos != null && _board[nextPos.X, nextPos.Y] != null && _board[nextPos.X, nextPos.Y].CurrentColor == sourcePiece.CurrentColor)
            {
                foreach (var ptf in piecesToFlip)
                {
                    ptf.Flip();
                }
            }
        }

        public string GetParameterString()
        {
            StringBuilder sb = new StringBuilder();
            for (int y = 0; y < BOARD_HEIGHT; ++y)
            {
                for (int x = 0; x < BOARD_WIDTH; ++x)
                {
                    if (_board[x, y] == null)
                    {
                        sb.Append("-");
                    }
                    else
                    {
                        switch (_board[x, y].CurrentColor)
                        {
                            case Color.BLACK:
                                sb.Append("B");
                                break;
                            case Color.WHITE:
                                sb.Append("W");
                                break;
                        }
                    }
                }
            }
            return sb.ToString();
        }

        public override string ToString()
        {
            StringBuilder sb = new StringBuilder();
            for (int y = -1; y < BOARD_HEIGHT; ++y)
            {
                for (int x = -1; x < BOARD_WIDTH; ++x)
                {
                    if (y < 0 || x < 0)
                    {
                        if (x < 0)
                        {
                            if (y >= 0)
                                sb.Append(y);
                            else
                                sb.Append(' ');
                        }
                        else
                        {
                            sb.Append(x);
                        }
                    }
                    else
                    {
                        if (_board[x, y] == null)
                        {
                            sb.Append("-");
                        }
                        else
                        {
                            switch (_board[x, y].CurrentColor)
                            {
                                case Color.BLACK:
                                    sb.Append("B");
                                    break;
                                case Color.WHITE:
                                    sb.Append("W");
                                    break;
                            }
                        }
                    }

                }
                if (y < BOARD_HEIGHT - 1)
                {
                    sb.AppendLine();
                }
            }
            return sb.ToString();
        }
    }
}
